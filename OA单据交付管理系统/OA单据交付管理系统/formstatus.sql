/*
Navicat MySQL Data Transfer

Source Server         : conn
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : formstatus

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-11-15 10:07:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for formstatus
-- ----------------------------
DROP TABLE IF EXISTS `formstatus`;
CREATE TABLE `formstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formid` varchar(50) DEFAULT NULL,
  `committime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(10) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of formstatus aaaa
-- ----------------------------
INSERT INTO `formstatus` VALUES ('19', 'FKSQ201612200128', '2019-11-13 17:30:24', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('20', 'FKSQ201612200128', '2019-11-13 17:37:19', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('21', 'FKSQ201611240042', '2019-11-14 09:55:04', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('22', 'FKSQ201611150002', '2019-11-14 09:58:10', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('23', 'FKSQ201611150002', '2019-11-14 10:08:35', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('24', 'ad', '2019-11-14 13:15:28', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('41', 'FKSQ201611150002', '2019-11-14 15:22:24', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('42', 'FKSQ201611150002', '2019-11-14 15:22:32', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('43', 'FKSQ201611240042', '2019-11-14 15:23:33', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('44', 'FKSQ201611150001', '2019-11-14 15:24:02', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('45', 'FKSQ201611150001', '2019-11-14 15:24:32', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('46', 'FKSQ201611150001', '2019-11-14 15:24:58', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('47', 'FKSQ201611240042', '2019-11-14 15:25:20', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('48', 'FKSQ201611170014', '2019-11-14 15:26:01', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('49', 'FKSQ201611170014', '2019-11-14 15:26:51', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('50', 'CCSQGN201611210010', '2019-11-14 15:28:12', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('51', 'CCSQGN201611210010', '2019-11-14 15:28:16', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('52', 'CCSQGN201611210010', '2019-11-14 15:28:25', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('53', 'PTBX201611140001', '2019-11-14 15:29:48', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('54', 'PTBX201611140001', '2019-11-14 15:29:51', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('55', 'YWZDF201612130026', '2019-11-14 15:30:55', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('56', 'YWZDF201612130026', '2019-11-14 15:30:58', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('57', 'YWZDF201612270039', '2019-11-14 15:31:30', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('58', 'YWZDF201612270039', '2019-11-14 15:31:34', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('59', 'YWZDF201612270039', '2019-11-14 15:31:56', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('60', 'CCSQGN201611210010', '2019-11-14 15:39:29', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('61', 'CCSQGN201611210010', '2019-11-14 15:39:31', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('62', 'CCSQGN201611210010', '2019-11-14 15:39:48', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('63', 'CCSQGN201611210009', '2019-11-14 15:40:19', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('64', 'CCSQGN201611210009', '2019-11-14 15:40:22', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('65', 'CCSQGN201611160004', '2019-11-14 15:40:51', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('66', 'CCSQGN201611160004', '2019-11-14 15:40:54', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('67', 'CCSQGN201611160004', '2019-11-14 15:41:15', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('68', 'PTBX201611140001', '2019-11-14 15:42:39', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('69', 'PTBX201611140001', '2019-11-14 15:42:42', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('70', 'PTBX201611140002', '2019-11-14 15:43:11', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('71', 'PTBX201611140002', '2019-11-14 15:43:33', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('72', '00', '2019-11-14 15:45:20', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('73', '00', '2019-11-14 15:59:08', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('74', 'adada', '2019-11-14 15:59:19', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('75', 'CCSQGN201611210010', '2019-11-14 15:59:44', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('76', 'CCSQGN201611210010', '2019-11-14 15:59:52', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('77', 'CCSQGN201611210010', '2019-11-14 16:00:06', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('78', 'CCSQGN201611210010', '2019-11-14 16:02:08', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('79', 'CCSQGN201611210010', '2019-11-14 16:03:11', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('80', 'CCSQGN201611210010', '2019-11-14 16:06:23', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('81', 'CCSQGN201611210010', '2019-11-14 16:08:14', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('82', 'CCSQGN201611210010', '2019-11-14 16:08:19', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('83', 'CCSQGN201611210010', '2019-11-14 16:08:21', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('84', 'CCSQGN201611210010', '2019-11-14 16:08:24', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('85', 'CCSQGN201611210010', '2019-11-14 16:08:45', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('86', 'CCSQGN201611210010', '2019-11-14 16:08:57', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('87', 'CCSQGN201611210010', '2019-11-14 16:11:03', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('88', 'dwd', '2019-11-14 16:11:09', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('89', 'CCSQGN201611210010', '2019-11-14 16:11:14', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('90', 'dwadawdad', '2019-11-14 16:11:19', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('91', '00', '2019-11-14 16:11:26', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('92', '00', '2019-11-14 16:11:28', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('93', 'CCSQGN201611210010', '2019-11-14 16:11:35', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('94', '1dwdw', '2019-11-14 16:16:10', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('95', 'CCSQGN201611210010', '2019-11-14 16:16:19', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('96', 'CCSQGN201611210010', '2019-11-14 16:16:30', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('97', 'YWZDF201612130026', '2019-11-14 16:16:49', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('98', 'YWZDF201612270039', '2019-11-14 16:17:05', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('99', 'YWZDF201612270039', '2019-11-14 16:17:11', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('100', 'YWZDF201612270039', '2019-11-14 16:17:15', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('101', 'YWZDF201612270039YWZDF201612270039', '2019-11-14 16:17:22', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('102', 'YWZDF201612270039', '2019-11-14 16:17:25', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('103', 'YWZDF201612270039', '2019-11-14 16:17:33', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('104', 'vYWZDF201612270039', '2019-11-14 16:17:37', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('105', 'YWZDF201612270039YWZDF201612270039', '2019-11-14 16:17:41', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('106', 'YWZDF201612270039YWZDF201612270039', '2019-11-14 16:17:45', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('107', 'YWZDF201612270039', '2019-11-14 16:17:49', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('108', 'vYWZDF201612270039', '2019-11-14 16:17:54', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('109', 'YWZDF201611220001', '2019-11-14 16:18:11', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('110', 'YWZDF201611220001', '2019-11-14 16:18:16', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('111', 'YWZDF201611220001', '2019-11-14 16:18:23', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('112', 'PTBX201611140001', '2019-11-14 16:18:40', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('113', 'PTBX201611140003', '2019-11-14 16:19:00', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('114', 'PTBX201611140003', '2019-11-14 16:19:25', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('115', 'PTBX201611140003', '2019-11-14 16:19:32', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('116', 'YWZDF201611220001', '2019-11-14 16:20:45', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('117', 'PTBX201611140006', '2019-11-14 16:21:49', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('118', 'PTBX201612060138', '2019-11-14 16:23:14', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('119', 'PTBX201611140001', '2019-11-14 16:23:58', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('120', 'YWZDF201611220002', '2019-11-14 16:24:50', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('121', 'YWZDF201611220002', '2019-11-14 16:25:20', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('122', 'YWZDF201612130026', '2019-11-14 16:28:48', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('123', 'FKSQ201611240042', '2019-11-14 16:29:09', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('124', 'FKSQ201611150002', '2019-11-14 16:30:34', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('125', 'FKSQ201611150010', '2019-11-14 16:30:44', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('126', 'FKSQ201612020067', '2019-11-14 16:31:09', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('127', 'FKSQ201612020067', '2019-11-14 16:31:28', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('128', 'FKSQ201612020067', '2019-11-14 16:31:35', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('129', 'CCSQGN201611210010', '2019-11-14 16:32:15', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('130', 'CCSQGN2016112100103', '2019-11-14 16:32:22', '提交失败', '提交失败，单号不存在！');
INSERT INTO `formstatus` VALUES ('131', 'CCSQGN201611210010', '2019-11-14 16:32:29', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('132', 'CCSQGN201611210012', '2019-11-14 16:32:59', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('133', 'CCSQGN201611210012', '2019-11-14 16:33:18', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('134', 'CCSQGN201611160003', '2019-11-14 16:33:43', '提交成功', '提交成功');
INSERT INTO `formstatus` VALUES ('135', 'CCSQGN201611160003', '2019-11-14 16:33:53', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('136', 'CCSQGN201611160003', '2019-11-14 16:33:58', '提交失败', '提交失败，原因：单据已提交，不能重复提交！');
INSERT INTO `formstatus` VALUES ('137', 'rera', '2019-11-15 09:52:05', '提交失败', '提交失败，单号不存在！');
