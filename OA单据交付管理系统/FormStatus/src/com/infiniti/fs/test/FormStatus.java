/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.test;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * @author liutao
 * @version 1.0
 */
public class FormStatus {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormStatus window = new FormStatus();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FormStatus() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 853, 508);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("单号");
		label.setBounds(14, 13, 37, 18);
		frame.getContentPane().add(label);
		
		textField = new JTextField();
		textField.setBounds(76, 10, 356, 24);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
	}
}
