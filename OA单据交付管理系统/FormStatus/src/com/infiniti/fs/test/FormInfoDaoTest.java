/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.test;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.javassist.compiler.ast.NewExpr;
import org.apache.log4j.Logger;
import org.junit.Test;

import com.infiniti.fs.dao.FormInfoDao;
import com.infiniti.fs.model.DfpInfo;
import com.infiniti.fs.model.FormInfo;

/**
 * @author liutao
 * @version 1.0
 */
public class FormInfoDaoTest {
	
	FormInfoDao FormInfoDao = new FormInfoDao();
	Logger logger = Logger.getLogger(FormInfoDao.class);
	
	@Test
	public void testInsertFormInfo() {
		FormInfo formInfo = new FormInfo("cs12","cs12","cs2");
		int flag = FormInfoDao.insertFormInfo(formInfo);
		logger.info(flag);
	}
	
	@Test
	public void testSelectFormInfo() {
		List<FormInfo> list = FormInfoDao.selectFormInfoCondition(null);
		logger.info(list);
	}
	
	@Test
	public void testCheckFormId() {
		String formid = "CNUH5006992q";
		String ssString = FormInfoDao.checkFormId("PTBX201611140001", "formtable_main_15", "ph");
		logger.info(ssString);
	}
	
	@Test
	public void testUpdateStatus() {
		String formidString = "FKSQ201611150002";
		String tableString = "formtable_main_13";
		String formidColumn = "fksqh";
		String isDFPString = "1";
		DfpInfo dfpInfo = new DfpInfo(formidString,tableString,formidColumn,isDFPString);
		int flag = FormInfoDao.updateStatus(dfpInfo);
		logger.info(flag);
	}
}
