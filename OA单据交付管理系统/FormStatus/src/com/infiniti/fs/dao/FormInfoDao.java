/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.dao;


import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;

import com.infiniti.fs.mapper.FormInfoMapper;
import com.infiniti.fs.model.DfpInfo;
import com.infiniti.fs.model.FormInfo;
import com.infiniti.fs.util.DBUtil;
import com.infiniti.fs.util.Dbutil2;

/**
 * @author liutao
 * @version 1.0
 */
public class FormInfoDao {
	/**
	 * 添加表单信息
	 * @param formInfo 表单信息
	 * @return 添加成功返回非零数字，否则返回0
	 */
	public int insertFormInfo(FormInfo formInfo) {
		int flag = 0;
		SqlSession session = null;
		try {
			session = Dbutil2.getSession();
			FormInfoMapper formInfoMapper = session.getMapper(FormInfoMapper.class);
			flag = formInfoMapper.insertFormInfo(formInfo);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeSession(session);
		}
		return flag;
	}
	/**
	 * 查询表格list
	 * @param map
	 * @return
	 */
	public List<FormInfo> selectFormInfoCondition(Map<String,Object> map) {
		List<FormInfo> list = new ArrayList<FormInfo>();
		SqlSession session = null;
		
		try {
			session = Dbutil2.getSession();
			FormInfoMapper formInfoMapper = session.getMapper(FormInfoMapper.class);
			list = formInfoMapper.selectFormInfoCondition(map);
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.closeSession(session);
		}
		return list;
	}
	/**
	 * 检查单号是否存在
	 * @param formid
	 * @param tableString
	 * @param formidString
	 * @return
	 */
	public String  checkFormId(String formidString,String tableString,String formidColumn) {
		String ss = null;
		SqlSession session = null;
		try {
			session = DBUtil.getSession();
			FormInfoMapper formInfoMapper = session.getMapper(FormInfoMapper.class);
			ss = formInfoMapper.checkFormId(formidString,tableString,formidColumn);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeSession(session);
		}
		return ss;
	}
	
	/**
	 * 判断是否带发票
	 * @param formidString 单据编号
	 * @return 返回一个字段（是否带发票）
	 */
	public String isDFP(String formidString) {
		String ss = null;
		SqlSession session = null;
		try {
			session = DBUtil.getSession();
			FormInfoMapper formInfoMapper = session.getMapper(FormInfoMapper.class);
			ss = formInfoMapper.isDFP(formidString);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeSession(session);
		}
		return ss;
	}
	
	/**
	 * 修改状态和提交时间
	 * @param formidString
	 * @param tableString
	 * @param formidColumn
	 * @return
	 */
	public int updateStatus(DfpInfo dfpInfo) {
		int flag = 0;
		SqlSession session = null;
		try {
			session = DBUtil.getSession();
			FormInfoMapper formInfoMapper = session.getMapper(FormInfoMapper.class);
			flag = formInfoMapper.updateStatus(dfpInfo);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeSession(session);
		}
		return flag;
	}
	
	
	/**
	 *  查询是否提交单据
	 * @param formidString
	 * @param tableString
	 * @param formidColumn
	 * @return
	 */
	public String isTJDJ(String formidString,String tableString,String formidColumn) {
		String ss = null;
		SqlSession session = null;
		try {
			session = DBUtil.getSession();
			FormInfoMapper formInfoMapper = session.getMapper(FormInfoMapper.class);
			ss = formInfoMapper.isTJDJ(formidString, tableString, formidColumn);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeSession(session);
		}
		return ss;
	}
	
}
