/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author liutao
 * @version 1.0
 */
public class Dbutil1 {
	public static Connection CONN()
	{
	String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";   //加载JDBC驱动
    String dbURL = "jdbc:sqlserver://DESKTOP-PK78SB6\\INSTANCE1;DatabaseName=FormStatuslt";   //连接服务器和数据库test
    String userName = "sa";   //默认用户名
    String userPwd = "123456";   //密码
    Connection dbConn=null;

    try
    {
    Class.forName(driverName);
    dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
    System.out.println("数据库连接成功！！");   //如果连接成功 控制台输出Connection Successful!
    } 
    catch (Exception e)
    {
    e.printStackTrace();
    }
    return dbConn;
}
	public static void main(String[] args) {
		CONN();
	}
}

