/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.util;



import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * 数据库连接实用类
 * @author liutao
 * @version 1.0
 */

public class Dbutil2 {
	public static SqlSessionFactory sessionFactory;

	static {
		try {
			// 使用MyBatis提供的Resources类加载mybatis的配置文件
			Reader reader = Resources.getResourceAsReader("mybatis-config2.xml");
			// 构建sqlSession的工厂
			sessionFactory = new SqlSessionFactoryBuilder().build(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 创建能执行映射文件中sql的sqlSession
	 * 
	 * @return 执行sql的sqlSession对象
	 */
	public static SqlSession getSession() {
		return sessionFactory.openSession();
	}

	/**
	 * 关闭session
	 * 
	 * @param session 要被关闭的session对象
	 */
	public static void closeSession(SqlSession session) {
		if (session != null) {
			session.close();
		}
	}
}
