/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.model;

/**
 * @author liutao
 * @version 1.0
 */
public class FormInfo {
	private int id;
	private String formid;
	private String committime;
	private String status;
	private String remark;
	
	public FormInfo() {
		super();
	}

	public FormInfo(int id, String formid, String committime, String status, String remark) {
		super();
		this.id = id;
		this.formid = formid;
		this.committime = committime;
		this.status = status;
		this.remark = remark;
	}

	public FormInfo(String formid, String committime, String status, String remark) {
		super();
		this.formid = formid;
		this.committime = committime;
		this.status = status;
		this.remark = remark;
	}
	
	

	public FormInfo(String formid, String status, String remark) {
		super();
		this.formid = formid;
		this.status = status;
		this.remark = remark;
	}
	
	public int getId() {
		return id;
	}

	public String getFormid() {
		return formid;
	}

	public String getCommittime() {
		return committime;
	}

	public String getStatus() {
		return status;
	}

	public String getRemark() {
		return remark;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFormid(String formid) {
		this.formid = formid;
	}

	public void setCommittime(String committime) {
		this.committime = committime;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "FormInfo [id=" + id + ", formid=" + formid + ", committime=" + committime + ", status=" + status
				+ ", remark=" + remark + "]";
	}
	
	
}
