/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.model;

/**
 * @author liutao
 * @version 1.0
 */
public class DfpInfo {
	private String formidString; 
	private String tableString; 
	private String formidColumn; 
	private String isDFPString;
	
	public DfpInfo(String formidString, String tableString, String formidColumn, String isDFPString) {
		super();
		this.formidString = formidString;
		this.tableString = tableString;
		this.formidColumn = formidColumn;
		this.isDFPString = isDFPString;
	}

	public DfpInfo() {
		super();
	}
	
	public String getFormidString() {
		return formidString;
	}

	public String getTableString() {
		return tableString;
	}

	public String getFormidColumn() {
		return formidColumn;
	}

	public String getIsDFPString() {
		return isDFPString;
	}

	public void setFormidString(String formidString) {
		this.formidString = formidString;
	}

	public void setTableString(String tableString) {
		this.tableString = tableString;
	}

	public void setFormidColumn(String formidColumn) {
		this.formidColumn = formidColumn;
	}

	public void setIsDFPString(String isDFPString) {
		this.isDFPString = isDFPString;
	}

	@Override
	public String toString() {
		return "DfpInfo [formidString=" + formidString + ", tableString=" + tableString + ", formidColumn="
				+ formidColumn + ", isDFPString=" + isDFPString + "]";
	} 
	
	
}
