/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.infiniti.fs.model.DfpInfo;
import com.infiniti.fs.model.FormInfo;
import com.oracle.webservices.internal.api.databinding.DatabindingMode;

/**
 * @author liutao
 * @version 1.0
 */
public interface FormInfoMapper {
	/**
	 * 添加表单信息
	 * @param formInfo 表单信息
	 * @return 添加成功返回非零数字否则返回0
	 */
	public int insertFormInfo(FormInfo formInfo);
	/**
	 * 条件组合查询
	 * @param map 条件集合
	 * @return 符合条件的表单信息集合
	 */
	public List<FormInfo> selectFormInfoCondition(Map<String,Object> map);
	
	/**
	 * 检查单号是否存在
	 * @param formidString 单据编号
	 * @param tableString 查询要去的表名
	 * @param formidColumn 查询所需的列名
	 * @return
	 */
	public String checkFormId(@Param("formidString")String formidString,@Param("tableString")String tableString,@Param("formidColumn")String formidColumn);
	
	/**
	 * 判断是否带发票
	 * @param formidString 单据编号
	 * @return 返回一个字段（是否带发票）
	 */
	public String isDFP(String formidString);
	
	/**
	 * 修改状态和提交时间
	 * @param formidString
	 * @param tableString
	 * @param formidColumn
	 * @return
	 */
	public int updateStatus(DfpInfo dfpInfo);
	
	
	/**
	 *  查询是否提交单据
	 * @param formidString
	 * @param tableString
	 * @param formidColumn
	 * @return
	 */
	public String isTJDJ(@Param("formidString")String formidString,@Param("tableString")String tableString,@Param("formidColumn")String formidColumn);
	
	
}
