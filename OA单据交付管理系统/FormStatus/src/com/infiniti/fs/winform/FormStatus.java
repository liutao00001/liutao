/**
 * CopyRight (C) YCKJ 2019.
 */
package com.infiniti.fs.winform;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.infiniti.fs.dao.FormInfoDao;
import com.infiniti.fs.model.DfpInfo;
import com.infiniti.fs.model.FormInfo;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JTable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Toolkit;

/**
 * @author liutao
 * @version 1.0
 */
public class FormStatus {
	FormInfoDao formInfoDao = new FormInfoDao();
	private JFrame frame;
	private JTextField formid;
	private JPanel panel;
	private JPanel totalpanel;
	private JPanel panel1;
	private JPanel toppanel;
	private JPanel jpaneltest;

	JScrollPane jScrollPane;
	private JPanel panel_up;
	String[] columnNames = { "单据编号", "提交时间", "提交状态", "备注" };

	/**
	 * 表格样式设置
	 */
	DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			if (row % 2 == 0) {
				setBackground(new Color(239, 238, 237));
			} else {
				setBackground(new Color(255, 255, 255));
			}
			setHorizontalAlignment(JLabel.CENTER);// 表格内容居中
			((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer())
					.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);// 列头内容居中
//                 table.getTableHeader().setFont(new Font("微软雅黑", Font.PLAIN, 13));
			table.getTableHeader().setResizingAllowed(true);
//                 table.setRowHeight(26);//设置行高
			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		}
	};

	List<FormInfo> list = formInfoDao.selectFormInfoCondition(null);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormStatus window = new FormStatus();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FormStatus() {
		initialize();
	}

	/**
	 * 将查询的得到的list转换为二维数组，渲染表格数据要用到二维数组
	 * 
	 * @return
	 */
	public JTable dataRef() {
		List<FormInfo> list = formInfoDao.selectFormInfoCondition(null);
		Object[][] data = new Object[list.size()][4];
		for (int i = 0; i < list.size(); i++) {
			FormInfo fm = list.get(i);
			data[i][0] = fm.getFormid();
			data[i][1] = fm.getCommittime();
			data[i][2] = fm.getStatus();
			data[i][3] = fm.getRemark();
		}
		DefaultTableModel tableModel = new DefaultTableModel(data, columnNames);// 渲染表格模型，data是二维数组，colomnNames为表头
		JTable table1 = new JTable(tableModel);

		Dimension size = table1.getTableHeader().getPreferredSize();
		size.height = 40;// 设置新的表头高度
		table1.getTableHeader().setFont(new Font("微软雅黑", Font.PLAIN, 16));// 表头字体
		table1.getTableHeader().setPreferredSize(size);
		// TableStyleUI.setTableHeaderColor(table1, 0, Color.RED);// 把表头的第1列设为红色
		for (int i = 0; i < table1.getColumnCount(); i++) {
			TableColumn col = table1.getColumn(table1.getColumnName(i));
			col.setCellRenderer(renderer);
			TableStyleUI.setTableHeaderColor(table1, i, new Color(235, 241, 241));
		}
		table1.setRowHeight(40);
		table1.getColumnModel().getColumn(0).setPreferredWidth(40);
		table1.getColumnModel().getColumn(1).setPreferredWidth(100);
		table1.getColumnModel().getColumn(2).setPreferredWidth(100);
		table1.getColumnModel().getColumn(3).setPreferredWidth(40);
		return table1;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("表单登记");
		panel = new JPanel();
		totalpanel = new JPanel();
		toppanel = new JPanel();
		panel1 = new JPanel();
		panel_up = new JPanel();
		panel_up.setLayout(new GridLayout(1, 1));
		panel_up.setBackground(Color.WHITE); // 先加个背景颜色
		panel_up.setOpaque(false); // 把panel的背景色设为透明
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.getContentPane().setLayout(null);
//		frame.getContentPane().setLayout(new BorderLayout());
//		frame.getContentPane().add(toppanel, BorderLayout.BEFORE_FIRST_LINE);
//		frame.getContentPane().add(panel1, BorderLayout.CENTER);

		frame.setUndecorated(true);// 窗体全屏
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle bounds = new Rectangle(screenSize);
		frame.setBounds(bounds);

		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setVisible(true);

		JLabel label_formid = new JLabel("单号： ");
		JLabel nullLabel = new JLabel("			");
		label_formid.setForeground(Color.WHITE);
		label_formid.setPreferredSize(new Dimension(60, 40));
		formid = new JTextField();
		formid.setPreferredSize(new Dimension(300, 40));
		
		java.net.URL topgroundimgURL = FormStatus.class.getResource("/images/logo4.png");
		ImageIcon topground = new ImageIcon(topgroundimgURL);
		JLabel topJLabel = new JLabel(topground);// 顶部区域
		toppanel.setBackground(Color.WHITE); // 先加个背景颜色
		toppanel.setOpaque(false); // 把panel的背景色设为透明
		toppanel.add(topJLabel);

		jpaneltest = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				java.net.URL backgroudimgURL = FormStatus.class.getResource("/images/backgroud.jpg");
				ImageIcon img = new ImageIcon(backgroudimgURL);
				
				img.paintIcon(this, g, 0, 0);
			}
		};
		jpaneltest.add(totalpanel);
		frame.add(jpaneltest);

		JPanel panel_small_1 = new JPanel();
		panel_small_1.setLayout(new FlowLayout());
		panel_small_1.add(label_formid);
		panel_small_1.add(formid);
		panel_small_1.add(nullLabel);

		panel_small_1.setBackground(Color.WHITE); // 先加个背景颜色
		panel_small_1.setOpaque(false); // 把panel的背景色设为透明
		panel_up.add(panel_small_1);

		panel1.setLayout(new BorderLayout());
		panel1.setBackground(Color.WHITE); // 先加个背景颜色
		panel1.setOpaque(false); // 把panel的背景色设为透明
		panel1.add(panel_up, BorderLayout.BEFORE_FIRST_LINE);
		panel1.add(panel, BorderLayout.CENTER);

		totalpanel.setLayout(new BorderLayout());
		totalpanel.setBackground(Color.WHITE); // 先加个背景颜色
		totalpanel.setOpaque(false); // 把panel的背景色设为透明
		totalpanel.add(toppanel, BorderLayout.BEFORE_FIRST_LINE);
		totalpanel.add(panel1, BorderLayout.CENTER);

		JTable table = dataRef();

		Action action = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String formidString = formid.getText();
				String tableString = "";
				String formidColumn = "";
				String status = "";
				String remark = "";

				String beginString = formidString.substring(0,2);
				if ("YW".equals(beginString)) {
					tableString = "formtable_main_17";
					formidColumn = "ph";
				}
				if ("PT".equals(beginString)) {
					tableString = "formtable_main_15";
					formidColumn = "ph";
				}
				if ("CC".equals(beginString)) {
					tableString = "formtable_main_10";
					formidColumn = "ph";
				}
				if ("FK".equals(beginString)) {
					tableString = "formtable_main_13";
					formidColumn = "fksqh";
				}
				System.out.println(beginString + "kaishi" + formidString + "--" + tableString + "++" + formidColumn
						+ "----------------------");
				// 查id是否存在，得到单据号
				String selectedFormIdString = formInfoDao.checkFormId(formidString, tableString, formidColumn);
				
				// 判断是否提交得到TJDJ字段的值 为0或1
				String isTJDJString = "";
				isTJDJString = formInfoDao.isTJDJ(selectedFormIdString, tableString, formidColumn);
				// 判断是否带发票 得到DFP字段
				String isDFPString = formInfoDao.isDFP(formidString);
				System.out.println("----------------"+isDFPString);
				// 单据号不为空就表明单据存在 若提交单据字段为否 就更改对应表中的字段和时间+提及成功；若为是，提示提交失败+单据已提交
				if (!(selectedFormIdString == null || selectedFormIdString.equals(""))) {
					// 若为是，提示提交失败+单据已提交
					if (isTJDJString!=null && ("1").equals(isTJDJString)) {
						status = "提交失败";
						remark = "提交失败，原因：单据已提交，不能重复提交！";
						FormInfo formInfo = new FormInfo(formidString, status, remark);
						formInfoDao.insertFormInfo(formInfo);// 添加一条记录
						List<FormInfo> list = formInfoDao.selectFormInfoCondition(null);// 获取新的数据集合
						Object[][] data = new Object[list.size()][4];
						for (int i = 0; i < list.size(); i++) {
							FormInfo fm = list.get(i);
							data[i][0] = fm.getFormid();
							data[i][1] = fm.getCommittime();
							data[i][2] = fm.getStatus();
							data[i][3] = fm.getRemark();
						}
						DefaultTableModel tableMode = new DefaultTableModel(data, columnNames);// 重新渲染一下表格数据
						table.setModel(tableMode);
						panel.setBackground(Color.WHITE); // 先加个背景颜色
						panel.setOpaque(false); // 把panel的背景色设为透明
						for (int i = 0; i < table.getColumnCount(); i++) {
							TableColumn col = table.getColumn(table.getColumnName(i));
							col.setCellRenderer(renderer);
//							TableStyleUI.setTableHeaderColor(table, i, new Color(235, 241, 241));
						}
						JOptionPane op = new JOptionPane("提交失败，单号已提交，不能重复提交!", JOptionPane.WARNING_MESSAGE);
						final JDialog dialog = op.createDialog("本提示框将在3秒后关闭");
						dialog.setFont(new Font("宋体", Font.PLAIN, 20));
						dialog.setBackground(Color.black);

						// 创建一个新计时器
						Timer timer = new Timer();
						// 3秒 后执行该任务
						timer.schedule(new TimerTask() {
							public void run() {
								dialog.setVisible(true);
								dialog.dispose();
							}
						}, 5000);

						dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
						dialog.setAlwaysOnTop(true);
						dialog.setModal(false);
						dialog.setVisible(true);
						dialog.setBounds(750, 380, 400, 200);
						formid.setText("");
					}
					// 否则可以提交，修改字段+提交成功
					else {
						status = "提交成功";
						remark = "提交成功";
						FormInfo formInfo = new FormInfo(formidString, status, remark);
						formInfoDao.insertFormInfo(formInfo);// 添加一条记录
						System.out.println("----------------"+isDFPString+"-----000000");
						DfpInfo dfpInfo = new DfpInfo(formidString, tableString, formidColumn, isDFPString);
						formInfoDao.updateStatus(dfpInfo);// 更改状态和时间
						List<FormInfo> list = formInfoDao.selectFormInfoCondition(null);// 获取新的数据集合
						Object[][] data = new Object[list.size()][4];
						for (int i = 0; i < list.size(); i++) {
							FormInfo fm = list.get(i);
							data[i][0] = fm.getFormid();
							data[i][1] = fm.getCommittime();
							data[i][2] = fm.getStatus();
							data[i][3] = fm.getRemark();
						}
						DefaultTableModel tableMode = new DefaultTableModel(data, columnNames);// 重新渲染一下表格数据
						table.setModel(tableMode);
						panel.setBackground(Color.WHITE); // 先加个背景颜色
						panel.setOpaque(false); // 把panel的背景色设为透明
						for (int i = 0; i < table.getColumnCount(); i++) {
							TableColumn col = table.getColumn(table.getColumnName(i));
							col.setCellRenderer(renderer);
//							TableStyleUI.setTableHeaderColor(table, i, new Color(235, 241, 241));
						}
						JOptionPane op = new JOptionPane("提交成功!", JOptionPane.WARNING_MESSAGE);
						final JDialog dialog = op.createDialog("本提示框将在3秒后关闭");
						dialog.setFont(new Font("宋体", Font.PLAIN, 20));
						dialog.setBackground(Color.black);

						// 创建一个新计时器
						Timer timer = new Timer();
						// 3秒 后执行该任务
						timer.schedule(new TimerTask() {
							public void run() {
								dialog.setVisible(true);
								dialog.dispose();
							}
						}, 5000);

						dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
						dialog.setAlwaysOnTop(true);
						dialog.setModal(false);
						dialog.setVisible(true);
						dialog.setBounds(750, 380, 400, 200);
						formid.setText("");
					}
				} else {
					status = "提交失败";
					remark = "提交失败，单号不存在！";
					FormInfo formInfo = new FormInfo(formidString, status, remark);
					formInfoDao.insertFormInfo(formInfo);// 添加一条记录
					List<FormInfo> list = formInfoDao.selectFormInfoCondition(null);// 获取新的数据集合
					Object[][] data = new Object[list.size()][4];
					for (int i = 0; i < list.size(); i++) {
						FormInfo fm = list.get(i);
						data[i][0] = fm.getFormid();
						data[i][1] = fm.getCommittime();
						data[i][2] = fm.getStatus();
						data[i][3] = fm.getRemark();
					}
					DefaultTableModel tableMode = new DefaultTableModel(data, columnNames);// 重新渲染一下表格数据
					table.setModel(tableMode);
					panel.setBackground(Color.WHITE); // 先加个背景颜色
					panel.setOpaque(false); // 把panel的背景色设为透明
					for (int i = 0; i < table.getColumnCount(); i++) {
						TableColumn col = table.getColumn(table.getColumnName(i));
						col.setCellRenderer(renderer);
//						TableStyleUI.setTableHeaderColor(table, i, new Color(235, 241, 241));
					}
//					JOptionPane.showMessageDialog(frame, "单号不存在!", "提示",JOptionPane.WARNING_MESSAGE); 
					JOptionPane op = new JOptionPane("提交失败，单号不存在!", JOptionPane.WARNING_MESSAGE);
					final JDialog dialog = op.createDialog("本提示框将在3秒后关闭");
					dialog.setFont(new Font("宋体", Font.PLAIN, 20));
					dialog.setBackground(Color.black);

					// 创建一个新计时器
					Timer timer = new Timer();

					// 3秒 后执行该任务
					timer.schedule(new TimerTask() {
						public void run() {
							dialog.setVisible(true);
							dialog.dispose();
						}
					}, 5000);

					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setAlwaysOnTop(true);
					dialog.setModal(false);
					dialog.setVisible(true);
					dialog.setBounds(750, 380, 400, 200);
					formid.setText("");
				}

			}
		};

		formid.addActionListener(action);

		jScrollPane = new JScrollPane(table) {
			@Override
			public Dimension getPreferredSize() {
				return new Dimension(1300, 850);
			}
		};
		jScrollPane.setBackground(Color.WHITE); // 先加个背景颜色
		jScrollPane.setOpaque(false); // 把panel的背景色设为透明
		panel.add(jScrollPane);
		panel.setBackground(Color.WHITE); // 先加个背景颜色
		panel.setOpaque(false); // 把panel的背景色设为透明
		frame.setVisible(true);
	}
}
